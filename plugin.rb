# name: user-menu
# about: Overrided User Menu Quick Access
# version: 0.0.1
# authors: Sergio Marreiro
register_asset "stylesheets/common/user-menu.scss";
register_asset "javascripts/initializers/user-menu.js.es6";