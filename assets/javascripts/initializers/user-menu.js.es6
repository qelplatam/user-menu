import { withPluginApi } from 'discourse/lib/plugin-api';
import { h } from "virtual-dom";

const QuickAccess = {
    BOOKMARKS: "bookmarks",
    MESSAGES: "messages",
    NOTIFICATIONS: "notifications",
    PROFILE: "profile",
};
let extraGlyphs;
let badges = [];
export function addUserMenuGlyph(glyph) {
    extraGlyphs = extraGlyphs || [];
    extraGlyphs.push(glyph);
}

function badge(number, selector) {
    if (number > 9) number = "+9";
    var span = document.querySelector(`${selector} span.notification-badge`) || document.createElement("span");
    span.classList.add("notification-badge");
    span.innerText = number;
    return span;
}


function initializePlugin(api) {
    api.reopenWidget("user-menu", {
        defaultState() {
            return {
                currentQuickAccess: QuickAccess.PROFILE,
                hasUnread: false,
                markUnread: null,
            };
        },
        didRenderWidget() {
            var user_notifications_link = document.querySelector(".user-notifications-link");
            var user_pms_link = document.querySelector(".user-pms-link");

            if (user_notifications_link && user_pms_link) {
                var hpn = Discourse.currentUser.unread_high_priority_notifications;
                var urn = Discourse.currentUser.unread_notifications;
                var upm = Discourse.currentUser.unread_private_messages;
                if (hpn > 0 || urn > 0) {
                    user_notifications_link.append(badge(hpn+urn, ".user-notifications-link"));
                }
                if (upm > 0) {
                    user_pms_link.append(badge(upm, ".user-pms-link"));
                }
            }
        }
    });
}

export default
    {
        name: 'user-menu',

        initialize(container) {
            withPluginApi('0.1', api => initializePlugin(api));
        }
    };